
public class Monom implements Comparable<Monom> {
	private Double coef=0.0;
	private int pow=0;

	public Monom(Double coef, int pow)
	{
		this.coef = coef;
		this.pow = pow;
	}

	Double getCoef()
	{
		return this.coef;	
	}

	int getPow()
	{
		return this.pow;	
	}
	void setCoef(Double coef)
	{
		this.coef = coef;
	}
	
	@Override
	public int compareTo(Monom o) {
		// TODO Auto-generated method stub
		if(this.pow < o.getPow())
			return 1;
		else if (this.pow > o.getPow())
			return -1;
		else return 0;
	}
	
	@Override
	public boolean equals(Object m1) {
		// TODO Auto-generated method stub
		if(Double.compare(this.getCoef(),((Monom)m1).getCoef())==0 && this.getPow()==((Monom)m1).getPow() )
			return true;
		else return false;
	}
}
