import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
	private Model theModel;
	private View theView;

	public Controller(Model theModel, View theView) {
		this.theModel = theModel;
		this.theView = theView;
		this.theView.addActionListener(new Calculate());
	}

	class Calculate implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				switch (theView.getComboBox()) {
				case "Addition":

					theView.setResult(theModel.adunare(new Polinom(theView.getP1()), new Polinom(theView.getP2()))
							.afisarePolinom());
					break;
				case "Subtraction":

					theView.setResult(theModel.scadere(new Polinom(theView.getP1()), new Polinom(theView.getP2()))
							.afisarePolinom());
					break;
				case "Multiplication":

					theView.setResult(theModel.inmultire(new Polinom(theView.getP1()), new Polinom(theView.getP2()))
							.afisarePolinom());

					break;
				case "Division":
					theView.setResult(theModel.impartire(new Polinom(theView.getP1()), new Polinom(theView.getP2()))
							.afisarePolinom());
					break;
				case "Integrate":

					theView.setResult(theModel.integrare(new Polinom(theView.getP1())).afisarePolinom());
					break;
				case "Derivate":

					theView.setResult(theModel.derivare(new Polinom(theView.getP1())).afisarePolinom());
					break;

				default:
					break;
				}

			} catch (NumberFormatException ex) {
				theView.setResult("Polinoamele nu au fost introduse corespunzator." + ex.getMessage());
			}
		}
	}

}
