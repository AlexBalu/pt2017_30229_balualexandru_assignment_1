import static org.junit.Assert.*;

public class Test {
	
	@org.junit.Test
	public void testSplit() {
		Polinom p1 = new Polinom("-x^2 +5*x -4");
		//p1.afisarePolinom();
		Polinom p2  = new Polinom();
		p2.getMonoms().add(new Monom(-1.0,2));
		p2.getMonoms().add(new Monom(5.0,1));
		p2.getMonoms().add(new Monom(-4.0,0));
		//p2.afisarePolinom();
		assert(p1.equals(p2));
	}
	@org.junit.Test
	public void adunare1()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("-6*x^4 + 2*x^3 + 7*x^2 -5");
		Polinom p2 = new Polinom("15*x^4 + 3*x^2 +5");
		Polinom p3 = new Polinom("9*x^4 +2*x^3 +10*x^2");
		assert(p3.equals(theModel.adunare(p1,p2)));
	}
	@org.junit.Test
	public void adunare2()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("17*x^100 -5*x^10");
		Polinom p2 = new Polinom("-x^100");
		Polinom p3 = new Polinom("16*x^100-5*x^10");
		assert(p3.equals(theModel.adunare(p1,p2)));
	}
	
	@org.junit.Test
	public void scadere1()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("17*x^100 -5*x^10");
		Polinom p2 = new Polinom("-x^100");
		Polinom p3 = new Polinom("18*x^100 - 5*x^10");
		assert(p3.equals(theModel.scadere(p1,p2)));
	}
	@org.junit.Test
	public void scadere2()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("15*x^4 + 3*x^2 +5");
		Polinom p2 = new Polinom("-6*x^4 + 2*x^3 + 7*x^2 -5");
		Polinom p3 = new Polinom("21*x^4 -2*x^3 -4*x^2 ");
		assert(p3.equals(theModel.scadere(p1,p2)));
	}
	
	@org.junit.Test
	public void inmultire1()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("15*x^4 + 3*x^2 +5");
		Polinom p2 = new Polinom("-6*x^4-5");
		Polinom p3 = new Polinom("-90*x^8 -18*x^6 - 75*x^4 -30*x^4 -15*x^2 -25");
		//p1.afisarePolinom();
		//p2.afisarePolinom();
		//p3.afisarePolinom();
		//theModel.inmultire(p1, p2).afisarePolinom();
		//assertTrue(true);
		assert(p3.equals(theModel.inmultire(p1,p2)));
	}
	
	@org.junit.Test
	public void inmultire2()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("3*x^2 -5*x+2");
		Polinom p2 = new Polinom("12*x^3");
		Polinom p3 = new Polinom("");
		assert(p3.equals(theModel.inmultire(p1,p2)));
	}
	
	@org.junit.Test
	public void impartire()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("8*x^5 -2*x^3 + 8*x^2 +7*x +1");
		Polinom p2 = new Polinom("-2*x + 3");
		Polinom p3 = new Polinom("-4*x^4-6*x^3 - 8*x^2 + 8*x - 8");
	    //p1.afisarePolinom();
		//p2.afisarePolinom();
		//p3.afisarePolinom();
		theModel.impartire(p1, p2).afisarePolinom();
		assert(true);
		//assert(p3.equals(theModel.impartire(p1, p2)));
	}
	
	@org.junit.Test
	public void impartire1()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("x^2");
		Polinom p2 = new Polinom("x");
		Polinom p3 = new Polinom("x");
		assert(p3.equals(theModel.impartire(p1, p2)));
	}
	
	@org.junit.Test
	public void derivare()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("x^6+5*x");
		Polinom p3 = new Polinom("6*x^5 + 5");
		assert(p3.equals(theModel.derivare(p1)));
	}
	
	@org.junit.Test
	public void integrare()
	{
		Model theModel = new Model();
		Polinom p1 = new Polinom ("x^6+5*x");
		Polinom p3 = new Polinom("0.1428*x^7 + 2.5*x^2");
		theModel.integrare(p1).afisarePolinom();
		assert(p3.equals(theModel.integrare(p1)));
	}
	
	

}
