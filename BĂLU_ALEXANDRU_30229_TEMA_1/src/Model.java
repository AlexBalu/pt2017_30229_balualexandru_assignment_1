import java.util.Collections;
import java.util.LinkedList;

public class Model {
	private Polinom result,rest; //rest only for division

	Model() {
		result = new Polinom();
		rest = new Polinom();
	}

	public Polinom adunare(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		

		for (Monom m1 : p1.getMonoms()) 
		{
			boolean gasit = false;
			for (Monom m2 : p2.getMonoms()) {
				if (m1.getPow() == m2.getPow()) {
					gasit = true;
					result.getMonoms().add(new Monom(m1.getCoef() + m2.getCoef(), m2.getPow()));
				}
			}
			
			if (!gasit) {
				result.getMonoms().add(new Monom(m1.getCoef(), m1.getPow()));
			}
		}

		for (Monom m1 : p2.getMonoms()) {
			boolean gasit = false;
			for (Monom m2 : p1.getMonoms())
				if (m1.getPow() == m2.getPow()) {
					gasit = true;
				}
			if (!gasit)
				result.getMonoms().add(new Monom(m1.getCoef(), m1.getPow()));
		}
		Collections.sort(result.getMonoms());
		result.simplifyPolynomial();
		// result.afisarePolinom();
		return result;
	}

	public Polinom scadere(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
	
		for (Monom m1 : p1.getMonoms()) {
			boolean gasit = false;
			for (Monom m2 : p2.getMonoms()) {
				if (m1.getPow() == m2.getPow()) {
					gasit = true;
					result.getMonoms().add(new Monom(m1.getCoef() - m2.getCoef(), m2.getPow()));
				}
			}
			if (!gasit) {
				result.getMonoms().add(new Monom(m1.getCoef(), m1.getPow()));
			}
		}

		for (Monom m1 : p2.getMonoms()) {
			boolean gasit = false;
			for (Monom m2 : p1.getMonoms())
				if (m1.getPow() == m2.getPow()) {
					gasit = true;
				}
			if (!gasit)
				result.getMonoms().add(new Monom(-m1.getCoef(), m1.getPow()));
		}

		Collections.sort(result.getMonoms());
		result.simplifyPolynomial();
		// result.afisarePolinom();
		return result;
	}

	public Polinom inmultire(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		for (Monom m1 : p1.getMonoms())
			for (Monom m2 : p2.getMonoms()) {
				result.getMonoms().add(new Monom(m1.getCoef() * m2.getCoef(), m1.getPow() + m2.getPow()));
			}
		Collections.sort(result.getMonoms());
		result.simplifyPolynomial();
		return result;
	}

	public Polinom impartire(Polinom p1, Polinom p2) {
		Polinom q, r;
		
		Double coef = 0.0;
		int pow = 0;
		q = new Polinom();
		p1.afisarePolinom();
		p2.afisarePolinom();
		r = p1;
		if (p1.getGrad() < p2.getGrad()) {
			System.out.println("The pow of the first polynomial should be bigger than the second one.");
			return result;
		}
		while (r.getGrad() >= p2.getGrad()) {
			coef = r.getMonomMax().getCoef() / p2.getMonomMax().getCoef();
			pow = r.getMonomMax().getPow() - p2.getMonomMax().getPow();
			q.getMonoms().add(new Monom(coef, pow));
			r = scadere(r, inmultire(p2, new Polinom(coef, pow)));
			r.simplifyPolynomial();
			//System.out.println("r=" + r.afisarePolinom() + " q=" + q.afisarePolinom());
		}
		System.out.println("cat = "+ q.afisarePolinom() );
		System.out.println( "rest= "+ r.afisarePolinom());
		result = q;
		rest = r;
		return result;
	}

	public Polinom derivare(Polinom p1) {
		Polinom deriv = new Polinom();
		for (Monom element : p1.getMonoms()) {
			if (element.getPow() > 1)
				deriv.getMonoms().add(new Monom(element.getCoef() * element.getPow(), element.getPow() - 1));
			else if (element.getPow() == 1)
				deriv.getMonoms().add(new Monom(element.getCoef(), 0));
		}
		// deriv.afisarePolinom();
		return deriv;
	}

	public Polinom integrare(Polinom p1) {
		Polinom intg = new Polinom();
		for (Monom element : p1.getMonoms()) {
			intg.getMonoms().add(new Monom(element.getCoef() / (element.getPow() + 1), element.getPow() + 1));
		}
		// intg.afisarePolinom();
		return intg;
	}

}
