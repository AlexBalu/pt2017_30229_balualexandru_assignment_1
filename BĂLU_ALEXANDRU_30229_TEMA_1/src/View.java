import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class View extends JFrame {
	private JTextArea p1 = new JTextArea("Introduce the first polynomial",1,20);
	private JTextArea p2 = new JTextArea("Introduce the second polynomial",1,20);
	private JTextArea res = new JTextArea(" ",1, 20);
	private JLabel l1 = new JLabel(" ");
	private JLabel l2 = new JLabel("+");
	private JLabel l3 = new JLabel("=");
	private JButton j1 = new JButton("Calculate");
	private JComboBox combo;

	View() {
		
		JPanel panel = new JPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1200, 200);
		
		p1.setLineWrap(true);
		p1.setWrapStyleWord(true);
		p2.setLineWrap(true);
		p2.setWrapStyleWord(true);
		res.setLineWrap(true);
		res.setWrapStyleWord(true);
		
		p1.setFont(new Font("Serif", Font.ITALIC, 20));
		p2.setFont(new Font("Serif", Font.ITALIC, 20));
		res.setFont(new Font("Serif", Font.ITALIC, 20));
		l1.setFont(new Font("Serif", Font.BOLD, 20));
		l2.setFont(new Font("Serif", Font.BOLD, 20));
		l3.setFont(new Font("Serif", Font.BOLD, 20));
		
		String[] patternExamples = { "Addition", "Subtraction", "Multiplication", "Division", "Integrate", "Derivate" };
		combo = new JComboBox<>(patternExamples);
		combo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
					res.setText("");
				switch (combo.getSelectedItem().toString()) {
				case "Addition":
					l1.setText("");
					l2.setText("+");
					l3.setText("=");
					p2.setVisible(true);
					break;
				case "Subtraction":
					l1.setText("");
					l2.setText("-");
					l3.setText("=");
					p2.setVisible(true);
					break;
				case "Multiplication":
					l1.setText("");
					l2.setText("*");
					l3.setText("=");
					p2.setVisible(true);
					break;
				case "Division":
					p2.setVisible(true);
					l1.setText("");
					l2.setText("/");
					l3.setText("=");

					break;
				case "Integrate":
					l1.setText("\u222B(");
					l2.setText(")");
					l3.setText("=");
					p2.setVisible(false);
					break;
				case "Derivate":
					p2.setVisible(false);
					l1.setText("(");
					l2.setText(")'");
					l3.setText("=");
					break;

				default:
					break;
				}

			}
		});
		panel.add(combo);
		panel.add(j1);
		panel.add(l1);
		panel.add(p1);
		
		p1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(p1.getText().equals("Introduce the first polynomial" ))
					p1.setText("");
			}
		});

		panel.add(l2);
		panel.add(p2);
		panel.add(l3);
		p2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(p2.getText().equals("Introduce the second polynomial" ))
				p2.setText("");
			}
		});
		panel.add(res);
		this.add(panel);

	}

	public String getP1() {
		return p1.getText();
	}

	public String getP2() {
		return p2.getText();
	}

	public void setResult(String par) {
		res.setText(par);
	}

	public void addActionListener(ActionListener p) {
		j1.addActionListener(p);
	}

	public String getComboBox() {
		return combo.getSelectedItem().toString();
	}

	public void setL1(String s) {
		l1.setText(s);
	}

	public void setL2(String s) {
		l2.setText(s);
	}

	public void setL3(String s) {
		l3.setText(s);
	}
}
