import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Polinom {

	private LinkedList<Monom> monoms;

	// polynom constructor - we get a string and we build the list of monoms
	public Polinom(String poli) {
		monoms = new LinkedList<Monom>();
		Double cf = 0.0;
		int pow = 0; // declare integers to temporarly save the
		// coefficient and the power
		String[] parts2, parts3;
		poli = poli.replace("-", "+-"); // to make the split after +
		poli = poli.replaceAll(" ", ""); // to remove " "

		// split the polynom String into monoms Strings and save them into the
		// list of monoms
		if (poli.equals("")) {
			this.monoms.add(new Monom(0.0, 0));
		} else {
			try {
				String[] parts = poli.split("\\+"); // split them using "+" sign
				for (String element : parts) {
					// System.out.println(element);
					if (element.length() == 0) // nothing to see here
						continue;

					// finding the coef
					if (element.charAt(0) == 'x') {
						cf = 1.0;
					} else if (element.length()>=2 && element.substring(0, 2).equals("-x")) {
							cf = -1.0;
					} else if(element.indexOf('*')>0){
						parts3 = element.split("\\*");
						cf = parts3.length > 1 ? Integer.parseInt(parts3[0])
								: element.indexOf('x') >= 0.0 ? 1.0 : Integer.parseInt(parts3[0]);
					}
					else
					{
						cf = Double.parseDouble(element);
					}

					// finding pow
					if(element.charAt(element.length()-1)=='x')
					{
						pow=1;
					}
					else if (element.indexOf('x')<0)
					{
						pow=0;
					}else
					{
					parts2 = element.split("\\^");
					pow = Integer.parseInt(parts2[1]);
					//pow = parts2.length > 1 ? Integer.parseInt(parts2[1]) : element.indexOf('x') >= 0 ? 1 : 0;
					}
					// System.out.println("coef: " +cf + " pow: "+pow);
					Monom m1 = new Monom(cf, pow);
					monoms.add(m1);
				}
			} catch (NumberFormatException e) {
				System.out.println(e.getMessage() + " Introduce the polynomial in the right way.");
			}
		}
	//	System.out.println("am facut un polinom: " + this.afisarePolinom());

		Collections.sort(this.monoms);
		this.simplifyPolynomial();
	}

	public Polinom() {
		this.monoms = new LinkedList<Monom>();
		
	}

	public Polinom(Double coef, int pow) {
		monoms = new LinkedList<Monom>();
		Monom m1 = new Monom(coef, pow);
		monoms.add(m1);
	}
	
	public int getGrad() {
		int max = 0;
		for (Monom element : this.monoms) {
			if (element.getPow() > max && element.getCoef() != 0)
				max = element.getPow();
		}
		return max;
	}

	public Monom getMonomMax() {
		Monom m = new Monom(0.0, 0);
		for (Monom element : this.monoms) {
			if (element.getPow() > m.getPow() && element.getCoef() != 0.0)
				m = element;
		}
		return m;
	}
	
	public String afisarePolinom() {
		String s = " ";
	//	DecimalFormat df = new DecimalFormat("####0.00");
		int zeroContor = 0;
		if (monoms.size()==0)
		{
			System.out.println("0");
			return "0 ";
		}
		for (Monom element : monoms) {

			//s = s + "+(" + df.format(element.getCoef()) + ")*x^" + element.getPow();
			s = s + "+(" + element.getCoef() + ")*x^" + element.getPow();

			/*
			 * if (element.getCoef() > 0) s = s + "+" +
			 * df.format(element.getCoef()); else s = s +
			 * df.format(element.getCoef());
			 * 
			 * if(element.getPow()==1) s=s+"*x"; else if(element.getPow()!=0)
			 * s=s+"*x^"+element.getPow(); else s = s+"";
			 */
		}
		// if the result is 0
		// if(zeroContor-1 == monoms.size())
		// s = "0";

		System.out.println(s);
		return s;

	}

	public void simplifyPolynomial() {
		//System.out.println("din simplify pol inainte de simplificare: " + this.afisarePolinom());
		LinkedList<Monom> temp = new LinkedList<Monom>();
		for (Monom m1 : this.getMonoms())
			for (Monom m2 : this.getMonoms()) {

				if (m1 != m2 && m1.compareTo(m2) == 0 && temp.contains(m2)==false && temp.contains(m1)==false) {
					m1.setCoef(m1.getCoef() + m2.getCoef());
					temp.add(m2);
				}
				
				if (Double.compare(m1.getCoef(), 0.0)==0)
					temp.add(m1);
				if (Double.compare(m2.getCoef(), 0.0)==0)
					temp.add(m2);
			}
		this.getMonoms().removeAll(temp);
		
		//System.out.println("din simplify pol dupa  simplificare:     " + this.afisarePolinom());
	}

	public List<Monom> getMonoms() {
		return this.monoms;
	}
	
	@Override
	public boolean equals(Object p1) {
		// TODO Auto-generated method stub
		this.simplifyPolynomial();
		((Polinom)p1).simplifyPolynomial();
		Collections.sort(this.getMonoms());
		Collections.sort(((Polinom)p1).getMonoms());
		ListIterator<Monom> i1 = ((Polinom)p1).getMonoms().listIterator();
	    ListIterator<Monom> i2 = this.getMonoms().listIterator();
	    while(i1.hasNext() && i2.hasNext())
	    {
	    	if((i1.next()).equals(i2.next())==false)
	    		return false;
	    }
		return true;
	}
	

}
